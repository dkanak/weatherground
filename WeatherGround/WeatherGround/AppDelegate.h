//
//  AppDelegate.h
//  WeatherGround
//
//  Created by Damian Kanak on 26/01/2015.
//  Copyright (c) 2015 mokan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

