//
//  main.m
//  WeatherGround
//
//  Created by Damian Kanak on 26/01/2015.
//  Copyright (c) 2015 mokan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
